# my-notebooks



## Getting started

Welcome to my Notebooks


## Where to Work

Once you have cloned the repository, enter the repository, create your own branch and checkout your branch.

```bash
cd my-notebooks
git branch <your_branch>
git checkout <your_branch>
```

## How to Enter the Docker Container

You can launch the Docker container by executing the following from this repository's root directory.

```bash
# my-notebooks/ $
./docker/run.sh
```

The Docker image should start now. Open the last link displayed in *your* terminal and JupyterLab will open in your browser.



